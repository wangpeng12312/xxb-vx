// index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({
    data: {
        imgurl: app.globalData.imgurl,
        tabitem: ['待确认', '已确认'],
        tabindex: 0,
        istask1: true,
        istask2: true,
        year: [],
        yearnum: '',
        index: 0,
        page1: 1,
        page2: 1,
        userInfor: {},
        tasklist1: [],
        tasklist2: [],
        allpage1: 1,
        pagenum1: 1,
        allpage2: 1,
        pagenum2: 1,

        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight
    },

    navback() {
        wx.navigateBack()
    },
    //   切换
    tabbtn(e) {
        var indexs = e.currentTarget.dataset.index
        this.setData({
            tabindex: indexs
        })
        if (indexs == 0) {
            this.setData({
                tasklist1: [],
                page1: 1
            })
            this.qytask()
        } else if (indexs == 1) {
            this.setData({
                tasklist2: [],
                page2: 1
            })
            this.tasktime()
            this.jstask()
        }
    },
    // 选择年限
    bindPickerChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value, Number(e.detail.value))
        var yeartime = this.data.year[Number(e.detail.value)]
        var task = []
        this.setData({
            index: e.detail.value,
            yearnum: yeartime,
            tasklist2: task
        })
        // console.log(this.data.index,yeartime,'====');
        this.jstask()
    },
    // 企业任务详情
    taskinfor(e) {
        console.log(e)
        var ids = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '/pages/wagesInfor/index?id=' + ids,
        })
    },
    qytask() {
        var that = this
        let data = {
            userId: that.data.userInfor.id,
            // userId:'1386193244446597121', 
            page: that.data.page1
        }
        $api.undeterminedTask(data).then((res) => {
            console.log(res, '待确认');
            if (res.code == 200) {
                var tasklists1 = that.data.tasklist1.concat(res.data.data.list);
                that.setData({
                    tasklist1: tasklists1,
                    allpage1: res.data.data.totalPage,
                    pagenum1: res.data.data.pageNumber
                })
                console.log(that.data.tasklist1, "==");
            } else {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                });
            }
        })
    },
    jstask() {
        var that = this
        let data = {
            userId: that.data.userInfor.id,
            // userId:'1386193244446597121',
            page: that.data.page2,
            year: that.data.yearnum
        }
        console.log(data, that.data.year);
        $api.settleTask(data).then((res) => {
            console.log(res, '已结算');
            if (res.code == 200) {
                var tasklist2 = that.data.tasklist2.concat(res.data.data.list);
                that.setData({
                    tasklist2: tasklist2,
                    allpage2: res.data.data.totalPage,
                    pagenum2: res.data.data.pageNumber
                })
            } else {
                  if(res.msg!="缺失必填参数"){
                                        wx.showToast({
                                            title: res.msg,
                                            icon: 'none'
                                        });
                                    }
            }
        })
    },
    tasktime() {
        var that = this;
        let data = {
            userId: that.data.userInfor.id,
            // userId:'1386193244446597121', 
        }
        $api.taskTime(data).then((res) => {
            console.log(res, '时间');
            var timearr = res.data.data
            var time = []
            if (res.code == 200) {
                for (const i in timearr) {
                    // console.log(timearr[i].wcsj);
                    time.push(timearr[i].createTime)
                }
                // console.log(time.reverse());
                that.setData({
                    year: time,
                    yearnum: time[0]
                })
            } else {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                });
            }
        })
    },

    onShow() {
        this.setData({
            tabindex: 0,
            page1: 1,
            page2: 1,
            tasklist1: [],
            tasklist2: [],
        })
        this.qytask()
        this.tasktime()
    },
    onLoad() {
        var that = this;
        that.setData({
            userInfor: wx.getStorageSync('userInfor')
        })
    },
    onReachBottom() {
        var that = this
        var all1 = that.data.allpage1;
        var page1 = that.data.pagenum1
        var all2 = that.data.allpage2;
        var page2 = that.data.pagenum2
        var index = that.data.tabindex

        if (index == 0) {
            console.log(page1, all1);
            if (page1 < all1) {
                that.setData({
                    page1: that.data.page1 + 1,
                })
                console.log(that.data.page1, '===');
                that.qytask()
            } else {
                // 这里直接给出提示就好了
                wx.showToast({
                    title: '暂无更多数据啦',
                    icon: 'none',
                    duration: 1000
                });
            }
        } else if (index == 1) {
            if (page2 < all2) {
                that.setData({
                    page2: that.data.page2 + 1,
                })
                that.jstask();
            } else {
                // 这里直接给出提示就好了
                wx.showToast({
                    title: '暂无更多数据啦',
                    icon: 'none',
                    duration: 1000
                });
            }
        }
    }
})
