// index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({
    data: {
        background: [
            {
                view:"../../static/index/banner1.png"
            },{
                view:"../../static/index/banner2.png"
            },{
                view:"../../static/index/banner3.png"
            },{
                view:"../../static/index/banner4.png"
            },{
                view:"../../static/index/banner5.png"
            },{
                view:"../../static/index/banner6.png"
            }
 
        ],
        indicatorDots: true,
        vertical: false,
        autoplay: false,
        interval: 2000,
        duration: 2000,
        imgurl: app.globalData.imgurl,
        motto: 'Hello World',
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        canIUseGetUserProfile: false,
        canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName'), // 如需尝试获取用户信息可改为false
        //获取全局变量 导航栏的高度statusBarHeight
        statusBarHeight: 0,
        userInfor: wx.getStorageSync('userInfor'),
        phone: ''
    },
    //   进入实名认证
    gorealAuth() {
        wx.navigateTo({
            url: '/pages/realAuth/index',
        })
    },
    // 电子签约
    gosign() {
        console.log(this.data.userInfor);
        if (this.data.userInfor.certification == 2) {
            wx.navigateTo({
                url: '/pages/signing/index',
            })
        } else {
            wx.showToast({
                title: '请实名后操作',
                icon: 'none'
            })
        }
    },
    // 银行卡列表
    gobankcard() {
        if (this.data.userInfor.certification == 2) {
            wx.navigateTo({
                url: '/pages/bankCard/index',
            })
        } else {
            wx.showToast({
                title: '请实名后操作',
                icon: 'none'
            })
        }

    },
    // 关注企业
    gzcompany() {
        wx.navigateTo({
            url: '/pages/company/index',
        })
    },
    // 任务结算
    tasksettle() {
        wx.navigateTo({
            url: '/pages/taskSettle/index',
        })
    },
    //   客服热线
    callPhone() {
        console.log(this.data.phone)
        var tel = this.data.phone
        wx.makePhoneCall({
            phoneNumber: tel //仅为示例，并非真实的电话号码
        })
    },

    // 常见问题
    goWt() {
        wx.navigateTo({
            url: '/pages/problem/index',
        })
    },
    // 事件处理函数
    bindViewTap() {
        wx.navigateTo({
            url: '../logs/logs'
        })
    },
    // 用户数据
    userdata() {
        var that = this;
        var tel = wx.getStorageSync('userInfor').tele
        let data = {
            tele: tel
        }
        $api.userInfor(data).then((res) => {
            console.log(res, '用户信息');
            if (res.code == 200) {
                wx.setStorageSync('userInfor', res.data.appAccount);
                that.setData({
                    userInfor: wx.getStorageSync('userInfor')
                })
            }
        })
    },
    onShow() {
        this.userdata()
    },
    onLoad() {
        var that = this;
        let data = {}
        $api.getPhone(data).then((res) => {
            console.log(res, '电话');
            that.setData({
                phone: res.data.data
            })
        })

        // 获取状态栏高度 
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    statusBarHeight: res.statusBarHeight
                })
            }
        })
        if (wx.getUserProfile) {
            this.setData({
                canIUseGetUserProfile: true
            })
        }
    },
    getUserProfile(e) {
        // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
        wx.getUserProfile({
            desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
                console.log(res)
                this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                })
            }
        })
    },
    getUserInfo(e) {
        // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
        console.log(e)
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true
        })
    },
    onShareAppMessage() {
        return {
          title: 'swiper',
          path: 'page/component/pages/swiper/swiper'
        }
      },
    
     
      changeIndicatorDots() {
        this.setData({
          indicatorDots: !this.data.indicatorDots
        })
      },
    
      changeAutoplay() {
        this.setData({
          autoplay: !this.data.autoplay
        })
      },
    
      intervalChange(e) {
        this.setData({
          interval: e.detail.value
        })
      },
    
      durationChange(e) {
        this.setData({
          duration: e.detail.value
        })
      }
})
