// pages/news/index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        imgurl: app.globalData.imgurl,
        banklist: [],
        banklogo: 'https://banklogo.yfb.now.sh/resource/logo/CMB.png',
        userInfor: {},

        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight
    },
    navback() {
        wx.navigateBack()
    },
    //   绑定银行卡
    bindbank() {
        wx.navigateTo({
            url: '/pages/bindBankCard/index',
        })
    },
    // 银行卡列表
    bankcard() {
        var that = this;
        var tel = that.data.userInfor.tele
        let data = {
            phoneNumber: tel,
        }
        $api.findBankCard(data).then((res) => {
            console.log(res, '银行卡列表');
            var bankobj = res.data.data
            for (let i in bankobj) {
                console.log(bankobj[i].bankNumber.length);
                bankobj[i].bankNumberF = bankobj[i].bankNumber.substr(bankobj[i].bankNumber.length - 4, bankobj[i].bankNumber.length)
            }
            console.log(bankobj);
            that.setData({
                banklist: bankobj
            })
        })
    },
    // 设为默认银行卡
    defabank(e) {
        console.log(e);
        var that = this;
        var bankid = e.currentTarget.dataset.id
        var appuserid = e.currentTarget.dataset.appuserid
        var tel = that.data.userInfor.tele
        // var tel = '13935062754'
        console.log(tel);
        wx.showModal({
            title: '设为默认?',
            content: '默认卡为您的提现卡以及收款卡',
            success: function (res) {
                let data = {
                    bankId: bankid,
                    appUserId: appuserid,
                    phoneNumber: tel
                }
                $api.setDefault(data).then((res) => {
                    console.log(res, '设置默认银行卡');
                    if (res.code == 200) {
                        wx.showToast({
                            title: "设置成功",
                            icon: 'none'
                        })
                        that.bankcard()
                    }
                })
            }
        });
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.setData({
            userInfor: wx.getStorageSync('userInfor')
        })
       
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.bankcard()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})