// pages/news/index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl:app.globalData.imgurl,
    userInfor:{},
    facedata:{},
    orderNumber:'',//订单编号
    identifyTime:'',
    identifyRet:''
  },
//   進入人臉識別
    facebtn(){
        var orderNumber = this.data.orderNumber 
        wx.navigateToMiniProgram({
            appId: 'wxfc1e27e05fdd062b',
            // path: 'pages/verify/info/index',
            path:'pages/verify/info/index?id='+orderNumber,
            // extraData: {
            //     id: orderNumber
            // },
            envVersion: 'release',
            success(res) {
                // 打开成功
            }
        }) 
    },
    // 获取订单编号
    getorderid(facedatas){
        var that = this;  
        console.log(facedatas,"获取订单编号数据");
        wx.request({
            url:'https://letsp.tspsp.com/api/api/face/getFaceQRCode', 
            method:'POST',
            header: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${facedatas.access_token}`    
            },
            data: {        
                companyTaxNumber : facedatas.tax_id, //企业税号
                // access_token : that.facedata.access_token, //访问令牌
                realName : facedatas.name, //真是姓名
                idCard : facedatas.idNo, //身份证号码
                needQrCode : "N",   
                idPhotoUp: wx.getStorageSync('idPhotoUp'),   //身份证正面照片
                idPhotoDown: wx.getStorageSync('idPhotoDown'),  //身份证反面照片 
                
                // companyTaxNumber : "91140100MA0H8NM33F", //企业税号
                // access_token : "ef6af14e-450f-46f1-8f 62-f496d3877916", //访问令牌
                // realName : "***", //真是姓名
                // idCard : "****************", //身份证号码
                // needQrCode : "N"
            }, 
            success:function(res){
                console.log(res,'订单编号');  
                that.setData({
                    orderNumber:res.data.data.orderNumber
                }) 
            }
        }) 
    },
    // 获取活体检测结果
    getresult(){
        var that = this;
        console.log(that.data.facedata,"获取活体结果参数")
        wx.request({
            url:'https://letsp.tspsp.com/api/api/face/getTpFaceVerifyResult', 
            method:'GET', 
            data: { 
                companyTaxNumber : that.data.facedata.tax_id, //企业税号 
                access_token : that.data.facedata.access_token, //访问令牌
                realName : that.data.facedata.name, //真是姓名
                idCard : that.data.facedata.idNo, //身份证号码  
                // companyTaxNumber : "55140100MA0H8N9901", //企业税号
                // access_token : "26b4f20c-d5d8-400a-8173-3f94bf769bdc", //访问令牌
                // realName : "郝琳珠", //真是姓名
                // idCard : "140429199805295624", //身份证号码  
            }, 
            success:function(res){
                console.log(res,'税务局活体结果')
                if(res.data.code==="0"){
                    var str=res.data.data;
                    var resultdata= JSON.parse(str)
                    // console.log(resultdata,"检测结果转数组"); 
                    that.setData({
                        identifyTime:resultdata.identifyTime,
                        identifyRet:resultdata.identifyRet
                    })
                    // console.log(that.identifyTime, resultdata.identifyTime);
                    // console.log(that.identifyRet,resultdata.identifyRet); 
                    let data = { 
                        identify_ret:resultdata.identifyRet,     
                        code:res.data.code,             
                        order_number:resultdata.orderNumber,    
                        app_user_id:that.data.userInfor.id     
                    }; 
                    $api.saveResults(data).then((result) => {
                        console.log(res,'本地认证结果上传'); 
                        if (result.code == 200) {
                            // that.userdata() 
                            wx.showToast({
                                title:'已实名认证成功！'
                            }) 
                            setTimeout(function(){  wx.navigateBack({  delta: 3 }) },3000) 
                        } else {
                            wx.showToast({ 
                                title: result.msg,
                                icon: 'none'
                            }); 
                        } 
                    })  
                    
                }
            }
        }) 
    },
    // 用户数据
    // userdata(){
    //     var that = this;
    //     var tel = wx.getStorageSync('userInfor').tele
    //     let data = {
    //         tele:tel
    //     } 
    //     $api.userInfor(data).then((res) => {
    //         console.log(res,'用户信息'); 
    //         if(res.code==200){
    //             wx.setStorageSync('userInfor', res.data.appAccount);
    //             that.setData({
    //                 userInfor: wx.getStorageSync('userInfor')
    //             }) 
    //         }
    //     }) 
    // },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log(options.rzdata);
    console.log(JSON.parse(options.rzdata)); 
    var facedatas = JSON.parse(options.rzdata)
    that.setData({
        userInfor: wx.getStorageSync('userInfor'),
        facedata:facedatas
    })   
    that.getorderid(facedatas)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getresult()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})