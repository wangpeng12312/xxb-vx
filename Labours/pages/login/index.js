// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    imgurl:app.globalData.imgurl,
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') ,// 如需尝试获取用户信息可改为false 
  },
    //   手机登录
    logintel(){
        wx.navigateTo({
          url: '/pages/loginPhone/index',
        })
    },
  getPhoneNumber(e){
      let code = e.detail.code;
      console.log(e)
      wx.login({
        success(res) {
          var codeLogin = res.code
          //var encryptedData = e.detail.encryptedData
          //var iv = e.detail.iv   
          if (codeLogin) { 
            wx.request({
                url:app.globalData.url+'/vx/login', 
                method:'POST',
                data:{
                    code: code,
                    codeLogin:codeLogin,
                    //encryptedData: encryptedData,
                    //iv: iv
                },
                success:function(res){
                    if(res.data.code=="200"){
                        console.log(res.data.data.token,"token信息：");
                        wx.setStorageSync('token', res.data.data.token);
                        wx.setStorageSync('openid', res.data.data.openid); 
                        wx.setStorageSync('userInfor', res.data.data.appAccount); 
                        // wx.redirectTo({
                        //     url: '/pages/index/index',
                        // })
                        wx.switchTab({
                            url: '/pages/index/index',
                        })
                    }  
                }
            }) 
          }
        }
      })
    },
      /** 
      wx.login({
        success(res) {
          console.log(res)
          wx.setStorageSync('code')
          var code = res.code
          var encryptedData = e.detail.encryptedData
          var iv = e.detail.iv   
          if (res.code) { 
            wx.request({
                url:app.globalData.url+'/vx/login', 
                method:'POST',
                data:{
                    code: code,
                    encryptedData: encryptedData,
                    iv: iv
                },
                success:function(res){
                    console.log(res,"dsf")
                    if(res.data.code=="200"){
                        wx.setStorageSync('token', res.data.data.token);
                        wx.setStorageSync('openid', res.data.data.openid); 
                        wx.setStorageSync('userInfor', res.data.data.appAccount); 
                        // wx.redirectTo({
                        //     url: '/pages/index/index',
                        // })
                        wx.switchTab({
                            url: '/pages/index/index',
                        })
                    }  
                }
            }) 
          }
        }
      }) */
    //   this.getuserinfo()
  //},
//   getuserinfo(e){ 
//         // console.log(e,app.globalData.url);//可以获取到个人的信息、加密偏移数据、加密用户信息 
//         wx.login({
//             success(res) {
//               console.log(res)
//               wx.setStorageSync('code')
//               var code = res.code
//               if (res.code) {
//                 wx.getUserInfo({
//                   success: function (res) {
//                     console.log(res,code)
//                     var encryptedData = res.encryptedData
//                     var iv = res.iv 
//                     // 3.解密用户信息 获取unionId
//                     wx.request({
//                       url:app.globalData.url+'/vx/login', 
//                       method:'POST',
//                       data:{
//                         code: code,
//                         encryptedData: encryptedData,
//                         iv: iv
//                       },
//                       success:function(res){
//                         console.log(res,"dsf")
//                         if(res.data.code=="200"){
//                             wx.setStorageSync('token', res.data.data.token);
//                             wx.setStorageSync('openid', res.data.data.openid); 
//                             wx.redirectTo({
//                               url: '/pages/index/index',
//                             })
//                         } 
//                         // console.log(wx.getStorageSync('token'));
//                         // wx.setStorageSync('sessionKey', res.data.userinfo.session_key)
//                         // that.setData({
//                         //   openid:res.data.userinfo.openid,
//                         //   canIUseGetUserProfile:true
//                         // })
//                       }
//                     })
//                   },
//                   fail: function () {
//                     console.log('获取用户信息失败')
//                   }
//                 })
      
//               }
//             }
//           })
//     },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true,
          canIUseGetUserProfile:false
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
