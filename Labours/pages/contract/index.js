// pages/news/index.js
// 获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: { 
    imgurl:app.globalData.imgurl,
    // srcurl:'https://sys.lgkj365.com/pdfjs/contractApplets.html?dispatchId=1497147040982376450'
    srcurl:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.dispatchId);
    // var fileurl = app.globalData.webhtml+'/pdfjs/contract.html?dispatchId='+options.dispatchId 
    let sj = Number(new Date());
    var fileurl = app.globalData.webhtml+'/pdfjs/contract.html?sj='+sj+'&dispatchId='+options.dispatchId
    console.log(fileurl);
    this.setData({
        srcurl:fileurl
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})