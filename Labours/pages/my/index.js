// pages/my/index.js
const app = getApp() 
const $api = require('../../utils/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl:app.globalData.imgurl,
    statusBarHeight: 0,
    staytask:[],
    userInfor:{}, 
    apiurl:app.globalData.url,
    allprice:''
  },
//   跳转到关于我们
    goabout:function(){
        wx.navigateTo({
          url: '/pages/aboutUs/index',
        })
    },
    settlement:function(){
        wx.navigateTo({
          url: '/pages/taskSettle/index',
        })
    },
    // 设置页面
    goset(){
        wx.navigateTo({
          url: '/pages/set/index',
        })
    }, 
    // 登录
    gologins(){
        wx.navigateTo({
          url: '/pages/login/index',
        })
    },
    getinfor(){
        console.log("getusu"); 
    },
    // 累计年收入
    getallMoney(userid){
        let data = {
            appuserId:userid
        } 
        $api.allMoney(data).then((res) => {
            console.log(res,'收入金额'); 
            if(res.code==200){
                this.setData({ 
                    allprice: res.data.basePay, 
                })
            }
           
        }) 
    },
    // 企业任务详情
    taskinfor(e){
        console.log(e)
        var ids = e.currentTarget.dataset.id
        wx.navigateTo({
          url: '/pages/wagesInfor/index?id='+ids,
        })
    },
    qytask(){
        var that = this 
        let data = {
            userId: that.data.userInfor.id,  
            // userId:'1386193244446597121', 
            page:1
        } 
        $api.undeterminedTask(data).then((res) => {
            console.log(res,'待确认');
            if(res.code==200){ 
                that.setData({
                    staytask:res.data.data.list, 
                })   
            }else {
                wx.showToast({ 
                    title: res.msg,
                    icon: 'none'
                }); 
            }  
        }) 
    },
     // 用户数据
  userdata(){
    var that = this;
    var tel = wx.getStorageSync('userInfor').tele
    let data = {
        tele:tel
    } 
    $api.userInfor(data).then((res) => {
        console.log(res,'用户信息'); 
        if(res.code==200){
            wx.setStorageSync('userInfor', res.data.appAccount);
            that.setData({
                userInfor: wx.getStorageSync('userInfor')
            }) 
        }
    }) 
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this; 
      that.setData({
          userInfor: wx.getStorageSync('userInfor')
      }) 
    that.getinfor();
    // that.qytask();
    that.getallMoney(that.data.userInfor.id)
    // 获取状态栏高度 
    wx.getSystemInfo({
     success: function (res) { 
       that.setData({
         statusBarHeight:res.statusBarHeight
       }) 
     } 
   }) 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.userdata()
    this.qytask()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})