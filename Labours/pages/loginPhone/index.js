// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    imgurl:app.globalData.imgurl,
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') ,// 如需尝试获取用户信息可改为false 
    tel:'',
    code:'',
    bizId:'',
    second: 60,
    phoneNumber: "",
    code1: true,
  },
    // 获取手机验证码
    getPhoneCode () {
        let that = this;
        let mobile = that.data.tel;
        console.log(mobile)
        if (!(/^1[23456789]\d{9}$/.test(mobile))) {
            wx.showToast({
                title: '手机号码有误',
                duration: 2000,
                icon: 'none'
            });
            return false;
        }
        if (that.data.code1) {
            var second = that.data.second; 
            that.setData({
                code1: false,
                loginStatic: true
            });
            var time = setInterval(()=> {
                if (second > 0) {
                second = second - 1;
                that.setData({
                    second: second
                });
                } else {
                that.setData({
                    code1: true,
                    second: 60,
                })
                clearInterval(time);
                }
            }, 1000);
            wx.request({
                url:app.globalData.url+'/vx/sendLoginSms', 
                method:'POST',
                data:{
                    phoneNumber:mobile
                },
                success:function(res){
                    
                    console.log(res,"验证码")
                    if(res.data.code=="200"){
                        let id = res.data.data.bizId
                        that.setData({
                            bizId: id
                        });
                        wx.showToast({
                            title: '验证码发送成功',
                            icon: 'none',
                            duration: 800
                        })
                    } else if(res.data.code=="500"){
                        wx.showToast({
                            title: res.data.msg,
                            icon: 'none', 
                        })
                    }
                }
            }) 
        }
     }, 
    // 手机号登录
    logintel(){
        var that = this;
        let mobile = that.data.tel;
        let code = that.data.code;
        let codeid = that.data.bizId;
        wx.request({
            url:app.globalData.url+'/vx/loginByPhone', 
            method:'POST',
            data:{
                phoneNumber:mobile,
                checkCode:code,
                bizId:codeid
            },
            success:function(res){
                console.log(res,"手机号登录")
                if(res.data.code=="200"){
                    wx.setStorageSync('token', res.data.data.token);
                    wx.setStorageSync('userInfor', res.data.data.appAccount); 
                    wx.switchTab({
                        url: '/pages/index/index',
                    })
                }  else{
                    wx.showToast({
                      title: res.errMsg,
                      icon:'none'
                    })
                }
            }
        }) 
    },
  gettel(e){  
      this.setData({
        tel:e.detail.value
      })
      console.log(this.data.tel)
      
  },
  getcode(e){ 
    this.setData({
        code:e.detail.value
    })
},
  getPhoneNumber(e){
      console.log(e);
      this.getuserinfo()
  },
  getuserinfo(e){ 
        console.log(e.detail);//可以获取到个人的信息、加密偏移数据、加密用户信息 
        wx.login({
            success(res) {
              console.log(res)
              wx.setStorageSync('code', res.code)
              var code = res.code
              if (res.code) {
                wx.getUserInfo({
                  success: function (res) {
                    console.log(res)
                    var encryptedData = res.encryptedData
                    var iv = res.iv
                    //3.解密用户信息 获取unionId
                    // wx.request({
                    //   'url':app.globalData.url+'/index/user/GetUid',
                    //   data:{
                    //     code: code,
                    //     encryptedData: encryptedData,
                    //     iv: iv
                    //   },
                    //   success:function(res)
                    //   {
                    //     console.log(res.data)
                    //     wx.setStorageSync('sessionKey', res.data.userinfo.session_key)
                    //     that.setData({
                    //       openid:res.data.userinfo.openid,
                    //       canIUseGetUserProfile:true
                    //     })
                    //   }
                    // })
                  },
                  fail: function () {
                    console.log('获取用户信息失败')
                  }
                })
      
              }
            }
          })
    },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
