// index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({
    data: {
        imgurl: app.globalData.imgurl,
        tabitem: ['签署合同', '过期合同'],
        tabindex: 0,
        isempty1: true,
        isempty2: false,
        deallist1: [],
        deallist2: [],
        userInfor: {},
        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight
    },
    navback() {
        wx.navigateBack()
    },
    //   切换
    tabbtn(e) {
        this.setData({
            tabindex: e.currentTarget.dataset.index
        })
        if (e.currentTarget.dataset.index == 0) {
            this.signupcontract()
        } else if (e.currentTarget.dataset.index == 1) {
            this.dqcontract()
        }
    },
    // 合同详情
    gocontract(e) {
        console.log(e, "id");
        var signAttachId = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '/pages/contract/index?dispatchId=' + signAttachId,
        })
    },
    // 签约到期合同列表
    dqcontract() {
        var that = this
        let data = {
            userId: that.data.userInfor.id,
            // userId:'1349938679409889281'
        }
        $api.expireContract(data).then((res) => {
            console.log(res, '到期合同');
            if (res.code == 200) {
                that.setData({
                    deallist2: res.data.data
                })
            } else {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                });
            }
        })
    },
    // 已签约合同
    signupcontract() {
        var that = this
        let data = {
            userId: that.data.userInfor.id,
        }
        $api.signupContract(data).then((res) => {
            console.log(res, '签约合同');
            if (res.code == 200) {
                that.setData({
                    deallist1: res.data.data
                })
            } else {
                // wx.showToast({
                //     title: res.msg,
                //     icon: 'none'
                // });
            }
        })
    },
    // 去签署
    faceAuth() {
        var that = this
        let data = {
            name: that.data.userInfor.name,
            thirdPartyUserId: that.data.userInfor.id,
            idCard: that.data.userInfor.idCard,
        }
        
        $api.electronicSigning(data).then((res) => {
            
            if (res.code == 500 || res.code == "500") {
                return wx.showToast({
                    title: res.msg,
                    icon: 'none'
                });
            } else if (res.code == 200 || res.code == "200") {
                wx.navigateTo({
                    url: `/pages/signContract/index?from=2&url=${encodeURIComponent(JSON.stringify(res.data.url))}`
                });
            } else {
                wx.showToast({
                    icon: 'none',
                    title: '加载失败',
                });
                return;
            }
        })
    },
    // 用户数据
    userdata() {
        var that = this;
        var tel = wx.getStorageSync('userInfor').tele
        let data = {
            tele: tel
        }
        $api.userInfor(data).then((res) => {
            console.log(res, '用户信息');
            if (res.code == 200) {
                wx.setStorageSync('userInfor', res.data.appAccount);
                that.setData({
                    userInfor: wx.getStorageSync('userInfor')
                })
            }
        })
    },
    onShow() {
        this.userdata()
    },
    onLoad() {
        var that = this;
        this.userdata()
        that.setData({
            userInfor: wx.getStorageSync('userInfor')
        })
        that.signupcontract()
    },
})
