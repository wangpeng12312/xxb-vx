// pages/news/index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl:app.globalData.imgurl,
    userInfor:{},
    news:[]
  },
  //   消息详情
  newinfor(e){
      console.log(e)
      var newid = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '/pages/newInfor/index?id='+newid,
        })
    },
    getnew(){
        console.log("消息请求");
        var that = this;
        let data = {
            phoneNumber:that.data.userInfor.tele
        } 
        $api.newList(data).then((res) => {
            console.log(res,'消息列表'); 
            this.setData({ 
                news: res.data.data, 
            })
        }) 
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      console.log('onLoad')
    var that = this;
    that.setData({
        userInfor: wx.getStorageSync('userInfor')
    }) 
    // that.getnew()
  },
    onPullDownRefresh:function(){
        this.onRefresh();
        console.log("下拉刷新");
    },
    onRefresh:function(){ 
        wx.showNavigationBarLoading();
        setTimeout(function () {
            wx.hideNavigationBarLoading();
            //停止下拉刷新
            wx.stopPullDownRefresh();
            this.getnew()
        }, 2000);
    }, 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getnew()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})