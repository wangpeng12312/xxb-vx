// index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({
    data: {
        listdata: [{
            id: 111,
            title: "为什么身份证会上传失败？",
            cont: ['请核对是否满足一下要求：身份证有效期需在60天以上；', '上传清晰身份证正反面，保证边框清晰可见，无闪光模糊或阴暗遮挡；', '年龄在18~75中间'],
            open: false
        }, {
            id: 222,
            title: "可以绑定几张银行卡？银行卡有什么要求？",
            cont: ['可以绑定多张银行卡，并设置收款顺序；银行卡请绑定一类储', '蓄卡，避免由于二类卡及三类卡收款限额影响；'],
            open: false
        }, {
            id: 333,
            title: "完成认证后多久可以收到业务费用？",
            cont: ['具体费用发放时间请联系与您合作的企业方进行确认；'],
            open: false
        }, {
            id: 444,
            title: "其他问题？",
            cont: ['如有其它问题，可电话垂直15536668712（周一至周六工；', '作时间9:00-18:00；'],
            open: false
        }],
        openvalue: 0,

        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight
    },
    navback() {
        wx.navigateBack()
    },

    changeitem(e) {
        let indexs = e.currentTarget.dataset.index
        let val = this.data.listdata[indexs].id === this.data.openvalue ? 0 : this.data.listdata[indexs].id
        this.setData({
            openvalue: val
        })
    }
})
