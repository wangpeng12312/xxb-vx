// pages/news/index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        imgurl: app.globalData.imgurl,
        qyId: '',
        qyMc: '',
        tel: '',
        userInfor: {},
        companylist: [],

        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight
    },

    navback() {
        wx.navigateBack()
    },
    //   扫码关注
    scancode() {
        var that = this;
        wx.scanCode({
            // onlyFromCamera: true,
            success(res) {
                console.log(JSON.parse(res.result));
                var cominfor = JSON.parse(res.result).qyId
                // var map = eval("(" + res.result + ")");
                var qyid = that.data.companylist
                // console
                for (var value of qyid) {
                    console.log(cominfor, value.qyId)
                    if (cominfor == value.qyId) {
                        console.log("--");
                        wx.showModal({
                            title: '提示',
                            content: '您已关注该企业',
                            success: function (res) {
                                res.cancel
                            }
                        });
                        return;
                    }
                }
                if (cominfor) {
                    console.log(res, cominfor, "=========")
                    var company = JSON.parse(res.result).qyMc
                    wx.showModal({
                        title: '企业名称',
                        content: company,
                        cancelText: '不关注',
                        confirmText: '关注',
                        success: function (res) {
                            if (res.confirm) {
                                let data = {
                                    tele: that.data.userInfor.tele,
                                    qyId: cominfor,
                                    qymc: company,
                                }
                                console.log(data);
                                $api.followEnterprise(data).then((res) => {
                                    // console.log(res,'关注公司');
                                    if (res.code == 200) {
                                        wx.showToast({
                                            title: '关注成功',
                                            icon: 'none'
                                        })
                                        that.getcomlist()
                                    } else {
                                        wx.showToast({
                                            title: res.msg,
                                            icon: 'none'
                                        });
                                    }
                                })
                            } else if (res.cancel) {
                                console.log('用户点击取消')
                            }
                        }
                    });
                }
            }
        })
    },
    getcomlist() {
        var that = this;
        let data = {
            appuserId: that.data.userInfor.id,
            // appuserId:'1386193244446597121'
        }
        // console.log(data);
        $api.enterpriseList(data).then((res) => {
            // console.log(res,'关注列表');
            if (res.code == 200) {
                that.setData({
                    companylist: res.data.data
                })
            } else {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                });
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.setData({
            userInfor: wx.getStorageSync('userInfor')
        })
        that.getcomlist()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})