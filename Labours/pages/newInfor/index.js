// index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({
    data: {
        imgurl: app.globalData.imgurl,
        newinfor: {},
        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight
    },
    navback() {
        wx.navigateBack()
    },
    getinfor(id) {
        var that = this;
        let data = {
            messageId: id
        }
        $api.newInfor(data).then((res) => {
            // console.log(res,'消息详情'); 
            this.setData({
                newinfor: res.data.data,
            })
        })
    },
    onLoad(option) {
        console.log(option.id)
        this.getinfor(option.id)
    },

})
