// pages/news/index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({
    /**
     * 页面的初始数据
     */
    data: {
        imgurl: app.globalData.imgurl, 
        sfz1: "../../static/index/add.png",
        sfz2: "../../static/index/add.png",
        id1: '',
        id2: '',
        name: '',   //名字
        usernum: '', //身份证号
        address: '',  //住址
        gender: '',   //性别
        nation: '',   //民族 
        idCardSignTime: '',   //身份证签约时间
        idCardExpiryTime: '',   //身份证过期时间
        validTime: '',
        office: '', //公安局归属地
        userInfor: {},
        access_token: '',//认证令牌
        tax_id: '',  //纳税号

        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight,
        checked:false
    },
    navback() {
        wx.navigateBack()
    },
    //   身份证正面
    successCallback(res) {
        console.log(res);
        var that = this;
        // if (that.userInfor.certification ==1 ) {
        //     return;
        // }
        console.log(res.detail.name.text, res.detail.id.text, "身份证正面");
        var base641 = 'data:image/jpg;base64,' + wx.getFileSystemManager().readFileSync(res.detail.image_path, "base64");
        var id1 = wx.getFileSystemManager().readFileSync(res.detail.image_path, "base64");
        wx.setStorageSync('idPhotoUp', base641);
        // console.log(base64);
        //res.detail.name=res.detail.name.text.replace(/\s/g,"");
        that.setData({
            // sfz1:res.detail.image_path,
            sfz1: base641,
            id1: id1,
            name: res.detail.name.text,
            usernum: res.detail.id.text,
            address: res.detail.address.text,
            gender: res.detail.gender.text,
            nation: res.detail.nationality.text
        })
    },
    // 身份证反面
    successback(res) {
        console.log(res, "身份证反面");
        // if (this.userInfor.certification == 2) {
        //     return;
        // }
        var base642 = 'data:image/jpg;base64,' + wx.getFileSystemManager().readFileSync(res.detail.image_path, "base64");
        var id2 = wx.getFileSystemManager().readFileSync(res.detail.image_path, "base64");
        wx.setStorageSync('idPhotoDown', base642);
        var cardtime = res.detail.valid_date.text
        var positive = cardtime.substring(0, 8)
        var back = cardtime.substring(9, 17)
        console.log(positive, back, "身份证时间")
        this.setData({
            // sfz2:res.detail.image_path,
            // validTime:res.detail.valid_date.text,
            sfz2: base642,
            id2: id2,
            office: res.detail.authority.text,
            idCardSignTime: positive,
            idCardExpiryTime: back,
        })
    },
    // 去人脸识别
    faceAuth() {
        // wx.navigateTo({
        //     url:'/pages/faceIdentify/index?rzdata=1' 
        // })
        var that = this;
        var infor = this.data
        if(!that.data.checked){
            return wx.showToast({
                title:"请阅读并勾选用户服务协议及隐私政策",
                icon:'none'
            })
        }

        var faceIdentify = {
            access_token: infor.access_token, //认证令牌
            tax_id: infor.tax_id,   //纳税号
            name: infor.name,
            idNo: infor.usernum,
            issuedBy: infor.office,   //签发机关
            idCardSignTime: infor.idCardSignTime, //身份证有效期开始日期 
            idCardExpiryTime: infor.idCardExpiryTime  //身份证有效期结束日期
        }
      
        if (infor.userInfor.certification == 2) {
            wx.showToast({
                title: infor.name+'已完成认证!',
                icon: 'none'
            });
            /**
            wx.navigateTo({
                // url:'/pages/faceIdentify/index?rzdata='+ encodeURIComponent(JSON.stringify(faceIdentify))
                url: '/pages/faceIdentify/index?rzdata=' + JSON.stringify(faceIdentify)
            }) */
        } else {
            if (!infor.name || !infor.usernum) {
                return wx.showToast({
                    title: '请上传身份证正面',
                    icon: 'none'
                })
            }
            if (!infor.office || !infor.idCardSignTime || !infor.idCardSignTime) {
                return wx.showToast({
                    title: '请上传身份证反面',
                    icon: 'none'
                })
            }
            let data = {
                name: infor.name,
                idNo: infor.usernum,
                id: infor.userInfor.id,
                frontImg: infor.id1,
                backImg: infor.id2,
                gender: infor.gender,
                address: infor.address,
                issuedBy: infor.office,
                idCardSignTime: infor.idCardSignTime,
                idCardExpiryTime: infor.idCardExpiryTime,
                nation: infor.nation
            }
            $api.authenticationName(data).then((res) => {
                if (res.code == 200) {
                    wx.showToast({
                        title: res.msg,
                        icon: 'none'
                    });
                    that.userdata();
                    wx.switchTab({
                      url:'/pages/index/index'
                    })
                    /**
                    wx.navigateTo({
                        // url:'/pages/faceIdentify/index?rzdata='+ encodeURIComponent(JSON.stringify(faceIdentify))
                        url: '/pages/faceIdentify/index?rzdata=' + JSON.stringify(faceIdentify)
                    }) */
                } else {
                    wx.showToast({
                        title: res.msg,
                        icon: 'none'
                    });
                }
            })

       }
    },
    // 获取认证令牌
    gettoken() {
        let data = {}
        $api.livingToken(data).then((res) => {
            console.log(res, '认证令牌');
            this.setData({
                access_token: res.data.token,
                tax_id: res.data.companyTaxNumber
            })
        })
    },
    // 用户数据
    userdata() {
        var that = this;
        var tel = this.data.userInfor.tele
        console.log(tel);
        let data = {
            tele: tel
        }
        $api.userInfor(data).then((res) => {
            console.log(res, '用户信息');
            if (res.code == 200) {
                wx.setStorageSync('userInfor', res.data.appAccount);
                that.setData({
                    userInfor: wx.getStorageSync('userInfor')
                })
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.setData({
            userInfor: wx.getStorageSync('userInfor')
        })
        that.gettoken()
        var userinfors = that.data.userInfor
        if (userinfors.certification == 2) {
            let sfz1 = app.globalData.url+"/"+userinfors.positive;
            let sfz2 = app.globalData.url+"/"+userinfors.back;
            that.setData({
                name: userinfors.name,
                usernum: userinfors.idCard,
                idCardSignTime: userinfors.idCardSignTime,
                idCardExpiryTime: userinfors.idCardExpiryTime,
                office: userinfors.issuedBy,
                sfz1: sfz1,
                sfz2: sfz2,
            })
            if (userinfors.liveState == 1) {
                console.log(`${app.globalData.url}/${userinfors.positive}`);
                wx.downloadFile({
                    url: `${app.globalData.url}/${userinfors.positive}`,//图片的地址
                    success: function (res) {
                        const tempFilePath = res.tempFilePath  //通过res中的tempFilePath 得到需要下载的图片地址
                        var base641 = 'data:image/jpg;base64,' + wx.getFileSystemManager().readFileSync(tempFilePath, "base64");
                        wx.setStorageSync('idPhotoUp', base641);
                        that.setData({
                            sfz1: base641,
                        })
                    }
                })
                wx.downloadFile({
                    url: `${app.globalData.url}/${userinfors.back}`,//图片的地址
                    success: function (res) {
                        const tempFilePath = res.tempFilePath  //通过res中的tempFilePath 得到需要下载的图片地址
                        var base642 = 'data:image/jpg;base64,' + wx.getFileSystemManager().readFileSync(tempFilePath, "base64");
                        wx.setStorageSync('idPhotoDown', base642);
                        that.setData({
                            sfz2: base642,
                        })
                    }
                })
            }
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

    checkboxChange:function(e){
        console.log('checkbox发生change事件，携带value值为：', e.detail.value)
        let val = e.detail.value
        console.log(this.data.checked);
        if(this.data.checked==false){
            this.setData({
                checked:true 
            })   
        }else{
            this.setData({
                checked:false 
            })  
        }
    },
    webxy() {
        wx.navigateTo({
            url: '/pages/webxy/index',
        })
    },
    webzc() {
        wx.navigateTo({
            url: '/pages/webzc/index',
        })
    },
})