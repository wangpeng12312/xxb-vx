// index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({
  data: {
    imgurl:app.globalData.imgurl,  
    isshow:false,
    xyisshow:false,
    checked: '', 
    details:{},
    dispatchId:''
  },
  //   确认订单
  suerorder(){
    this.setData({
        xyisshow:true
    })
},
    // 差异说明
    difference(){
        wx.showModal({
            title:"差异说明",
            content:'确认无误，提交货品交割订单！',
            success(){
                console.log("111");
            }
        })
    },
    // pdf页面
    continfor(e){
        var pdfid = e.currentTarget.dataset.id
        console.log(e);
        wx.navigateTo({
            url: '/pages/contract/index?dispatchId='+pdfid,
        })
    },
    // 发票pdf
    fpinfor(){
        wx.navigateTo({
        url: '/pages/invoicePdf/index',
        })
    },
    closeshow(){
        this.setData({
            isshow:false
        })
    },
    difference(){
        this.setData({
            isshow:true
        })
    },
    checkboxChange:function(e){
        console.log('checkbox发生change事件，携带value值为：', e.detail.value)
        let val = e.detail.value
        if(val.length>0){
        this.setData({
            checked:true 
        })
        }else{
        this.setData({
            checked:false 
        })
        }
    },
    // 同意零工业务协议合同
    agreexy:function(){
        var that = this;
        if(!that.data.checked){
            return wx.showToast({
                title:"请阅读并勾选猩猩邦业务协议",
                icon:'none'
            })
        }
        let data = {
            dispatchId: that.data.dispatchId,   
        } 
        $api.confirmTask(data).then((res) => {
            console.log(res,'确认任务');
            if(res.code==200){ 
                // 同意协议并确认生成合同
                wx.showToast({
                    title:'确认成功！',
                    icon:'none'
                })
                setTimeout(function(){
                    that.gettaskinfor(that.data.dispatchId)
                },2000)
                that.setData({
                    xyisshow:false
                })  
            }else {
                wx.showToast({ 
                    title: res.msg,
                    icon: 'none'
                }); 
            }  
        }) 
        
    },
    gettaskinfor(id){
        var that = this
        let data = {
            dispatchId: id,   
        } 
        $api.taskInfor(data).then((res) => {
            console.log(res,'详情');
            if(res.code==200){ 
                that.setData({
                    details:res.data.data, 
                })   
            }else {
                wx.showToast({ 
                    title: res.msg,
                    icon: 'none'
                }); 
            }  
        }) 
    },
 
  onShow(){  
  },
  onLoad(options) {
     this.gettaskinfor(options.id)
     this.setData({
        dispatchId:options.id
     })
  }, 
})
