// index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
Page({
    data: {
        imgurl: app.globalData.imgurl,

        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight
    },
    navback() {
        wx.navigateBack()
    },
    bindPickerChange: function (e) {
        this.setData({
            index: e.detail.value
        })
    },
    bindPickerChange1: function (e) {
        this.setData({
            index1: e.detail.value
        })
    },
    webxy() {
        wx.navigateTo({
            url: '/pages/webxy/index',
        })
    },
    webzc() {
        wx.navigateTo({
            url: '/pages/webzc/index',
        })
    },
    //   退出登录
    loginout() {
        wx.showModal({
            title: '',
            content: '是否确定退出当前账号?',
            success: function (res) {
                if (res.confirm) {
                    let userInfo =  wx.getStorageSync('userInfor');
                    let tele = userInfo.tele;
                    let userId = userInfo.id;
                    console.log(userInfo);
                    let data = {
                        token: wx.getStorageSync('token'),
                        tele:tele,
                        userId:userId
                    }
                    $api.loginOut(data).then((res) => {
                        // console.log(res,'退出登录'); 
                        if (res.code == 200) {
                            wx.removeStorageSync('userInfor');
                            wx.removeStorageSync('token');
                            wx.removeStorageSync('openid');
                            wx.removeStorageSync('idPhotoUp');
                            wx.removeStorageSync('idPhotoDown');
                            wx.reLaunch({
                                url: '/pages/login/index',
                            })
                        } else {
                            wx.showToast({
                                title: res.msg,
                                icon: 'none'
                            });
                        }
                    })
                }
            }
        })

    },

})
