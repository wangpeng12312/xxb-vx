// pages/news/index.js
// 获取应用实例
const app = getApp()
const $api = require('../../utils/index')
const bankCode = require('../../utils/bankCode')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        imgurl: app.globalData.imgurl,
        codetext: '发送验证码',
        authFalg: false,
        count: 60,
        banknum: '',
        bankname: '',
        userInfor: {},
        bizId: '',
        bankcode: '',
        telcode: '',
        statusBarHeight: app.globalData.statusBarHeight,
        navBarHeight: app.globalData.navBarHeight,
        checked:false
    },
    navback() {
        wx.navigateBack()
    },
    //   检验银行卡名称
    bankNamevier() {
        var that = this;
        var banknum = that.data.banknum.replace(/\s+/g, '')
        that.setData({
            banknum: banknum
        })
        if (banknum == '') {
            return wx.showToast({
                title: '输入银行卡后检验',
                icon: 'none'
            })
        }
        that.gainCode(banknum)
        let data = {
            bankNumber: banknum,
        }
        $api.searchBankName(data).then((res) => {
            // console.log(res,'检验银行卡'); 
            if (res.code == 200) {
                that.setData({
                    bankname: res.data.result.bankName
                })
            } else if (res.code == 500) {
                wx.showToast({
                    title: res.data.result.msg,
                    icon: 'none'
                })
            }
        })
    },
    //   发送验证码
    authClick() {
        var that = this;
        that.setData({
            codetext: that.data.count + '秒后重新获取',
            authFalg: true
        })
        var s = setInterval(function () {
            if (that.data.count > 1) {
                that.setData({
                    count: that.data.count - 1,
                    codetext: that.data.count + '秒后重新获取'
                })
            } else {
                that.setData({
                    authFalg: false,
                    count: 60,
                    codetext: "重新发送"
                })
                clearInterval(s);
            }
        }, 1000);
        let data = {
            phoneNumber: that.data.userInfor.tele,
        }
        $api.bindCardCode(data).then((res) => {
            console.log(res, '发送验证码');
            if (res.code == 200) {
                that.setData({
                    bizId: res.data.bizId
                })
            } else if (res.code == 500) {
                wx.showToast({
                    title: '发送失败',
                    icon: 'none'
                })
            }
        })
    },
    // 输入银行卡号
    banknuminput(res) {
        this.setData({
            banknum: res.detail.value
        })
    },
    // 输入手机验证码
    telcodes(res) {
        this.setData({
            telcode: res.detail.value
        })
    },
    // 银行卡卡号
    blankSuccess(res) {
        console.log(res, res.detail.number.text, '银行卡');
        this.setData({
            banknum: res.detail.number.text
        })
    },
    // 获取银行卡code
    gainCode(banknum) {
        var bcode = bankCode.bankCardAttribution(banknum)
        console.log(bcode.bankCode, '银行卡类型');
        this.setData({
            bankcode: bcode.bankCode
        })
    },
    // 绑定银行卡
    bindbanknum() {
        var that = this;
        if(!that.data.checked){
            return wx.showToast({
                title:"请阅读并勾选用户服务协议及隐私政策",
                icon:'none'
            })
        }
        var userid = that.data.userInfor.id;
        var banknum = that.data.banknum;
        var bankname = that.data.bankname;
        var bankcode = that.data.bankcode;
        var telcode = that.data.telcode;
        var bizid = that.data.bizId;
        var tel = that.data.userInfor.tele;
        if (!banknum) {
            return wx.showToast({ title: '请输入银行卡号', icon: 'none' })
        }
        if (!bankname) {
            return wx.showToast({ title: '请检验银行卡名称', icon: 'none' })
        }
        if (!telcode) {
            return wx.showToast({ title: '请输入验证码', icon: 'none' })
        }
        let data = {
            id: userid,
            cardNo: banknum,
            bankName: bankname,
            bankCode: bankcode,
            checkCode: telcode,
            bizId: bizid,
            phoneNumber: tel,
        }
        console.log(data);
        $api.bindBankCard(data).then((res) => {
            console.log(res, '绑定银行卡');
            if (res.code == 200) {
                wx.showToast({
                    title: '银行卡绑定成功',
                })
                setTimeout(function () {
                    wx.navigateBack({ delta: 1 });
                }, 800)
            } else if (res.code == 500) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(bankCode.bankCardAttribution(6227003320240034988));
        var that = this;
        that.setData({
            userInfor: wx.getStorageSync('userInfor')
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    checkboxChange:function(e){
        console.log('checkbox发生change事件，携带value值为：', e.detail.value)
        let val = e.detail.value
        console.log(this.data.checked);
        if(this.data.checked==false){
            this.setData({
                checked:true 
            })   
        }else{
            this.setData({
                checked:false 
            })  
        }
    },
    webxy() {
        wx.navigateTo({
            url: '/pages/webxy/index',
        })
    },
    webzc() {
        wx.navigateTo({
            url: '/pages/webzc/index',
        })
    },
})