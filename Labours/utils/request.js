const app = getApp()
const baseUrl = app.globalData.url
module.exports = {
    request : function(url, methodType, data){
        // console.log(data);
      let fullUrl = `${baseUrl}${url}`
      // let token = wx.getStorageSync('token') ? wx.getStorageSync('token')  : ''
      wx.showLoading({ title: "加载中",icon:'none'  });
      return new Promise((resolve,reject)=>{
        wx.request({
          url: fullUrl,
          method:methodType,
          data,
          header: {
            'content-type': 'application/json', // 默认值
            'token':  wx.getStorageSync('token')
          },
          success(res){ 
              console.log(res,res.data.code);
              if(res.data.code === '403'){
                    console.log("----");
                    wx.hideLoading()
                    wx.reLaunch({
                    url: '/pages/login/index',
                    })
                }
              resolve(res.data)
              wx.hideLoading()
            // if (res.statusCode == 200) {
            //   resolve(res.data)
            //   wx.hideLoading()
            // }else if(res.data.code === '403'){
            //     console.log("----");
            //     wx.hideLoading()
            //     wx.reLaunch({
            //       url: '/pages/login/index',
            //     })
            // }else{
            //   wx.hideLoading()
            // //   wx.showToast({
            // //     title: res.data.msg,
            // //     icon:'none'
            // //   })
            //   reject(res.data.message)
            // }
          },
          fail(){
            wx.showToast({
              title: '接口请求错误',
              icon:'none'
            })
            reject('接口请求错误')
          }
        })
      })
    }
  }