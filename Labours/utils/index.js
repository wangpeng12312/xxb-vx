import { request } from './request'
module.exports = {
    // 客服联系电话
    getPhone: (data) => request('/vx/setting/customerService', 'GET', data),
    // 退出登录
    loginOut:(data) => request('/vx/logout', 'POST', data),
    // 用户数据
    userInfor:(data) => request('/vx/setting/userDetail', 'POST', data),
    // 登录
    // /vx/login    POST
    // 手机号登录
    // /vx/loginByPhone    POST
    // 登录验证码
    // /vx/sendLoginSms     POST

    // 查询银行卡
    findBankCard: (data) => request('/vx/setting/findAllBankCard', 'POST', data),
    // 设置默认银行卡
    setDefault:(data) => request('/vx/setting/settingBankCard',"POST",data),
    // 检验银行卡名称
    searchBankName:(data) => request('/vx/setting/searchBankNameText',"POST",data),
    // 绑定银行卡验证码
    bindCardCode:(data) => request('/vx/setting/sendBindCardCode',"POST",data),
    // 绑定银行卡
    bindBankCard:(data) => request('/vx/setting/replaceBankNumber',"POST",data),

    // 提交实名认证
    authenticationName:(data) => request('/vx/setting/checkLiveByRealName',"POST",data),
    // 获取access_token认证令牌
    livingToken:(data) => request('/vx/setting/findCheckToken',"POST",data),
    // 获取订单编号  POST
    // https://letsp.tspsp.com/api/api/face/getFaceQRCode
    // 获取活体检测结果  GET
    // https://letsp.tspsp.com/api/api/face/getTpFaceVerifyResult
    // 保存活体结果
    saveResults:(data) => request('/vx/setting/saveCheckLiveRecord',"POST",data),

    // 电子签约到期合同
    expireContract:(data) => request('/vx/setting/findExpireContractByUserId',"POST",data),
    // 电子签约签约合同
    signupContract:(data) => request('/vx/setting/findContractByUserId',"POST",data),
    // 电子签约
    electronicSigning:(data) => request('/vx/setting/createAccount',"POST",data),

    // 关注企业列表
    enterpriseList:(data) => request('/vx/setting/findCompany',"POST",data),
    // 关注企业信息
    followEnterprise:(data) => request('/vx/setting/focusCompany',"POST",data),
    // 本年累计收入
    allMoney:(data) => request('/vx/setting/findCumulativeAmount',"POST",data),

    // 消息列表
    newList:(data) => request('/vx/setting/findMessage',"POST",data),
    // 消息详情
    newInfor:(data) => request('/vx/setting/findMessageInfo',"POST",data),

    // 待确认任务
    undeterminedTask:(data) => request('/vx/setting/getUnconfirmedMyTask',"POST",data),
    // 已结算任务
    settleTask:(data) => request('/vx/setting/getConfirmedMyTaskByTime',"POST",data),
    // 企业任务详情
    taskInfor:(data) => request('/vx/setting/getQyTask',"POST",data),
    // 任务时间
    taskTime:(data) => request('/vx/setting/getConfirmedMyTask',"POST",data),
    // 确认任务
    confirmTask:(data) => request('/vx/setting/confirmTask',"POST",data),

}